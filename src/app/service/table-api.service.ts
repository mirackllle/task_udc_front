import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Branch } from '../entity/branch';
import { map, catchError } from 'rxjs/operators';
import { Description } from '../entity/description';

@Injectable({
  providedIn: 'root'
})
export class TableApiService {

  constructor(private httpClient: HttpClient ) { }

  /**
  * method that get Description.
  * @returns Observable<Description>
  */
  getDescription(): Observable<Description> {
    return this.httpClient.get("description").pipe(
      map((description: Description) => {
        description.dateCr = new Date(description.dateCr);
        description.branches = description.branches.map(branch => {
          branch.datePay = new Date(branch.datePay);
          return branch;
        });
        return description;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      }) 
    );
  }

  /**
  * method that save exist or new Branch.
  * @returns Observable<Branch>
  */
  saveBranch(branch: Branch): Observable<Branch> { 
    if(branch.branchId) {
      return this.updateBranch(branch);
    }
    return this.addBranch(branch);
  }

  /**
  * method that delete Branch.
  * @returns Observable<Branch>
  */
  deleteBranch(id): Observable<any> {
    return this.httpClient.delete(`branch/delete/${id}`).pipe(
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      }) 
    );
  }

  private addBranch(branch): Observable<Branch> {
    return this.httpClient.post("branch/create", branch).pipe(
      map((branch: Branch) => {
        branch.datePay = new Date(branch.datePay);
        return branch;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      }) 
    );
  }

  private updateBranch(branch): Observable<Branch> {
    return this.httpClient.put("branch/update", branch).pipe(
      map((branch: Branch) => {
        branch.datePay = new Date(branch.datePay);
        return branch;
      }),
      catchError((error: HttpErrorResponse) => {
        return throwError(error);
      }) 
    );
  }


}
