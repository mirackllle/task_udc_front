import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CustomTableComponent } from './component/custom-table/custom-table.component';
import {
  MatButtonModule,
  MatDialogModule,
  MatGridListModule,
  MatInputModule,
  MatTableModule,
} from '@angular/material';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BranchModalComponent } from './component/branch-modal/branch-modal.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MainInterceptor } from './app.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    CustomTableComponent,
    BranchModalComponent
  ],
  entryComponents: [
    CustomTableComponent,
    BranchModalComponent
  ],
  imports: [
    BrowserModule,
    MatDialogModule,
    MatButtonModule,
    MatGridListModule,
    MatInputModule,
    MatTableModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: MainInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
