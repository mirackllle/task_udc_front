import { Component, OnInit } from '@angular/core';
import {Description} from '../../entity/description'
import { TableApiService } from '../../service/table-api.service';
import { Branch } from '../../entity/branch';
import { MatDialog } from '@angular/material';
import { BranchModalComponent } from '../branch-modal/branch-modal.component';

@Component({
  selector: 'app-custom-table',
  templateUrl: './custom-table.component.html',
  styleUrls: ['./custom-table.component.css']
})
export class CustomTableComponent implements OnInit {

  columnsToDisplay: string[] = ['Num', 'Sum', 'Lines', 'Date Pay', 'Num List', 'File Name', 'File Data'];
  displayedColumns: any[] = [
    {name: "Num", value: "branchNum"},
    {name: "Sum", value: "branchSum"},
    {name: "Lines", value: "branchLines"},
    {name: "Date Pay", value: "shortDatePay"},
    {name: "Num List", value: "numList"},
    {name: "File Name", value: "fileName"},
    {name: "File Data", value: "fileData"}
  ]
  description: Description;
  data;

  constructor(private tableService: TableApiService, public dialog: MatDialog) {}

  /**
  * method that initializes the table.
  */
  ngOnInit() {
    this.getDescription();
  }

  /**
  * method that open modal with new branch.
  */
  addBranch() {
    const branch = new Branch(null, null, null, new Date(), null, null, null, this.description.descriptionId, null);
    const dialogRef = this.dialog.open(BranchModalComponent, {
      width: '800px',
      data: branch
    });

    dialogRef.afterClosed().subscribe(_result => {
      this.getDescription();
    });
  }

  /**
  * method that open modal with selected branch.
  */
  editColumn(branch) {
    const dialogRef = this.dialog.open(BranchModalComponent, {
      width: '800px',
      data: branch
    });

    dialogRef.afterClosed().subscribe(_result => {
      this.getDescription();
    });
  }

  /**
  * method that get description.
  */
  getDescription() {
    this.tableService.getDescription().subscribe(
      (desc: Description) => {
        this.description = desc;
        this.data = desc.branches;
        this.data.forEach(elem => {
          elem.shortDatePay = elem.datePay.toDateString();
        })
      }
    )
  }

}
