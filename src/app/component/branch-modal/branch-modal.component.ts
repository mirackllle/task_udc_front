import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Branch } from '../../entity/branch';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TableApiService } from 'src/app/service/table-api.service';

@Component({
  selector: 'app-branch-modal',
  templateUrl: './branch-modal.component.html',
  styleUrls: ['./branch-modal.component.css']
})
export class BranchModalComponent implements OnInit {

  branchModalForm: FormGroup;
  
  constructor(private dialogRef: MatDialogRef<BranchModalComponent>, @Inject(MAT_DIALOG_DATA) private data: Branch,
    private formBuilder: FormBuilder, private tableApi: TableApiService) {}

  /**
  * method that init the form group.
  */
  ngOnInit(): void {
    this.branchModalForm = this.formBuilder.group({
      branch_id: [{value: this.data.branchId, disabled: true}, [Validators.required]],
      branch_lines: [{value: this.data.branchLines, disabled: false}, [Validators.required, Validators.min(0), Validators.max(100), Validators.pattern('^[0-9]+$')]],
      branch_num: [{value: this.data.branchNum, disabled: false}, [Validators.required, Validators.min(0), Validators.max(1000), Validators.pattern('^[0-9]+$')]],
      branch_sum: [{value: this.data.branchSum, disabled: false}, [Validators.required, Validators.min(0), Validators.pattern('^[0-9]+$')]],
      date_pay: [{value: this.data.datePay.toISOString().split('T')[0], disabled: false}, [Validators.required]],
      description_id: [{value: this.data.descriptionId, disabled: false}, [Validators.required]],
      file_data: [{value: this.data.fileData, disabled: false}, [Validators.required]],
      file_name: [{value: this.data.fileName, disabled: false}, [Validators.required]],
      num_list: [{value: this.data.numList, disabled: false}, [Validators.required, Validators.min(0), Validators.max(1000), Validators.pattern('^[0-9]+$')]],
    });
  }

  /**
  * method that save changes and create new branch.
  */
  onSave(): void {
    if (this.branchModalForm.valid) {
      const branch: Branch = new Branch(
        this.branchModalForm.controls.branch_num.value,
        this.branchModalForm.controls.branch_sum.value,
        this.branchModalForm.controls.branch_lines.value,
        new Date(this.branchModalForm.controls.date_pay.value),
        this.branchModalForm.controls.num_list.value,
        this.branchModalForm.controls.file_name.value,
        this.branchModalForm.controls.file_data.value,
        this.branchModalForm.controls.description_id.value,
        this.branchModalForm.controls.branch_id.value,
      );
      this.tableApi.saveBranch(branch).subscribe(
        (_branch) => {
          this.dialogRef.close();
        }
      );
      return;
    }
    console.log("branch not valid");
  }

  /**
  * method that remove branch.
  */
  onRemove(): void {
    this.tableApi.deleteBranch(this.branchModalForm.controls.branch_id.value).subscribe(
      (_resp) => {
        this.dialogRef.close();
      }
    )
  }

  /**
  * method that close branch.
  */
  onNoClick(): void {
    this.dialogRef.close();
  }

}
