export class Branch {
  branchNum: number;
  branchSum: number;
  branchLines: number;
  datePay: Date;
  numList: number;
  fileName: string;
  fileData: string;
  descriptionId: number;
  branchId: number;

  constructor(branchNum: number, branchSum: number, branchLines: number, datePay: Date, numList: number,
    fileName: string, fileData: string, descriptionId: number, branchId: number) {
      this.branchNum = branchNum;
      this.branchSum = branchSum;
      this.branchLines = branchLines;
      this.datePay = datePay;
      this.numList = numList;
      this.fileName = fileName;
      this.fileData = fileData;
      this.descriptionId = descriptionId;
      this.branchId = branchId;
  }
}