import { Branch } from "./branch";

export class Description {
  descriptionId: number;
  opfuCode: number;
  opfuName: string;
  dateCr: Date;
  mfoFilia: number;
  filiaNum: number;
  filiaName: string;
  fullSum: number;
  fullLines: number;
  branches: Branch[];

  constructor(descriptionId: number, opfuCode: number, opfuName: string, dateCr: Date, mfoFilia: number,
    filiaNum: number, filiaName: string, fullSum: number, fullLines: number, branches: Branch[]) {
      this.descriptionId = descriptionId;
      this.opfuCode = opfuCode;
      this.opfuName = opfuName;
      this.dateCr = dateCr;
      this.mfoFilia = mfoFilia;
      this.filiaNum = filiaNum;
      this.filiaName = filiaName;
      this.fullSum = fullSum;
      this.fullLines = fullLines;
      this.branches = branches;
  }
}