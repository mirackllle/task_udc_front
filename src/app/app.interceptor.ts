import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class MainInterceptor implements HttpInterceptor
{
    
    constructor()
    {

    }

    /**
    * method add dns to request.
    * @returns Observable<HttpEvent<any>>
    */
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>
    {
        let dns = 'http://localhost:8080/';
        const apiReq = req.clone({ url: `${dns}${req.url}` });
        return next.handle(apiReq);
    }

}
