import { of, Observable } from "rxjs";
import { Branch } from "src/app/entity/branch";
import { Description } from "src/app/entity/description";

export class TableApiServiceMock {

  testBranch = new Branch(1, 1, 1, new Date(), 1, '1', '1', 1, null);
  testDescription = new Description(1, 1, '1', new Date(), 1, 1, '1', 1, 1, [this.testBranch]);

  constructor() { }

  getDescription() {
    return {
      subscribe: (res) => {
        res(this.testDescription);
      }
    };
  }

  saveBranch(branch: Branch) { 
    return of({});
  }

  deleteBranch(id) {
    return of({});
  }


}
