import { TestBed } from '@angular/core/testing';

import { TableApiService } from '../../app/service/table-api.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Description } from 'src/app/entity/description';
import { Branch } from 'src/app/entity/branch';
import { HttpErrorResponse } from '@angular/common/http';

describe('TableApiService', () => {

  let tableApiService: TableApiService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TableApiService]
    })

    tableApiService = TestBed.get(TableApiService);
    httpTestingController = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    const service: TableApiService = TestBed.get(TableApiService);
    expect(service).toBeTruthy();
  });

  it('should be getDescription sucessfully', () => {
    const testBranch = new Branch(1, 1, 1, new Date(), 1, '1', '1', 1, null);
    const testDescription = new Description(1, 1, '1', new Date(), 1, 1, '1', 1, 1, [testBranch]);
    tableApiService.getDescription().subscribe(
      (description: Description) => {
        expect(description).toEqual(testDescription);
      }
    )
    const req = httpTestingController.expectOne('description');
    expect(req.request.method).toEqual('GET');
    req.flush(testDescription);
  });

  // it('should be getDescription error', () => {
  //   const testBranch = new Branch(1, 1, 1, new Date(), 1, '1', '1', 1, null);
  //   const testDescription = new Description(1, 1, '1', new Date(), 1, 1, '1', 1, 1, [testBranch]);
  //   const error = new HttpErrorResponse({error: 'Critical error'});
  //   console.log(tableApiService.getDescription())
  //   const req = httpTestingController.expectOne('description');
  //   expect(req.request.method).toEqual('GET');
  //   req.flush(error); 
  // });

  it('should be addBranch sucessfully', () => {
    const testBranch = new Branch(1, 1, 1, new Date(), 1, '1', '1', 1, null);
    tableApiService.saveBranch(testBranch).subscribe(
      (branch: Branch) => {
        expect(branch).toEqual(testBranch);
      }
    )
    const req = httpTestingController.expectOne('branch/create');
    expect(req.request.method).toEqual('POST');
    req.flush(testBranch);
  });

  it('should be aupdateBranch sucessfully', () => {
    const testBranch = new Branch(1, 1, 1, new Date(), 1, '1', '1', 1, 1);
    tableApiService.saveBranch(testBranch).subscribe(
      (branch: Branch) => {
        expect(branch).toEqual(testBranch);
      }
    )
    const req = httpTestingController.expectOne('branch/update');
    expect(req.request.method).toEqual('PUT');
    req.flush(testBranch);
  })

  it('should be deleteBranch sucessfully', () => {
    tableApiService.deleteBranch(1).subscribe();
    const req = httpTestingController.expectOne('branch/delete/1');
    expect(req.request.method).toEqual('DELETE');
    req.flush('1');
  })
});
