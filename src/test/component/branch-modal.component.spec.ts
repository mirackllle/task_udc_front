import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BranchModalComponent } from '../../app/component/branch-modal/branch-modal.component';
import { NO_ERRORS_SCHEMA, DebugElement } from '@angular/core';
import { TableApiService } from 'src/app/service/table-api.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableApiServiceMock } from '../service/table-api-service.mock';

import { Branch } from 'src/app/entity/branch';
import { By } from '@angular/platform-browser';

describe('BranchModalComponent', () => {
  let component: BranchModalComponent;
  let fixture: ComponentFixture<BranchModalComponent>;
  let debugElement: DebugElement;
  let tableApiService: TableApiService;
  let matDialogRef: MatDialogRef<any>;
  let tableApiSpySave;
  let tableApiSpyRemove;
  let dialogRefSpyCansel;
  const branch = new Branch(1, 1, 1, new Date(), 1, "1", "1", 1, 1);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BranchModalComponent ],
      schemas: [ NO_ERRORS_SCHEMA ],
      providers: [
        { provide: TableApiService, useClass: TableApiServiceMock },
        { provide: MatDialogRef, useValue: {close: () => {}} },
	      { provide: MAT_DIALOG_DATA, useValue: branch },
      ],
      imports: [
        FormsModule, 
        ReactiveFormsModule, 
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BranchModalComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;
    tableApiService = debugElement.injector.get(TableApiService);
    tableApiSpySave = spyOn(tableApiService, 'saveBranch').and.callThrough();
    tableApiSpyRemove = spyOn(tableApiService, 'deleteBranch').and.callThrough();
    matDialogRef = debugElement.injector.get(MatDialogRef);
    dialogRefSpyCansel = spyOn(matDialogRef, 'close').and.callThrough();;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be onCancel button click', () => {
    const cancelButton = fixture.debugElement.query(By.css('button'));
    cancelButton.triggerEventHandler('click', null);
    expect(dialogRefSpyCansel).toHaveBeenCalledTimes(1);
  });

  it('should be onSave button click, formValid', () => {
    const saveButton = fixture.debugElement.query(By.css('button:nth-of-type(2)'));
    saveButton.nativeElement.click();
    expect(component.branchModalForm.valid).toBeTruthy();
    expect(tableApiSpySave).toHaveBeenCalledTimes(1);
  });

  it('should be onSave button click, formInValid', () => {
    const saveButton = fixture.debugElement.query(By.css('button:nth-of-type(2)'));
    component.branchModalForm.controls['branch_sum'].setValue("")
    saveButton.nativeElement.click();
    expect(component.branchModalForm.valid).toBeFalsy();
    expect(tableApiSpySave).toHaveBeenCalledTimes(0);
  });


  it('should be onRemove button click', () => {
    const removeButton = fixture.debugElement.query(By.css('button:nth-of-type(3)'));
    removeButton.triggerEventHandler('click', null);
    expect(tableApiSpyRemove).toHaveBeenCalledTimes(1);
  });
});
