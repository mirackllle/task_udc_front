import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomTableComponent } from '../../app/component/custom-table/custom-table.component';
import { NO_ERRORS_SCHEMA, DebugElement } from '@angular/core';
import { TableApiService } from 'src/app/service/table-api.service';
import { MatDialog, MatTableModule } from '@angular/material';
import { TableApiServiceMock } from '../service/table-api-service.mock';
import { By } from '@angular/platform-browser';

describe('CustomTableComponent', () => {
  let component: CustomTableComponent;
  let fixture: ComponentFixture<CustomTableComponent>;
  let debugElement: DebugElement;
  let tableApiService: TableApiService;
  let matDialogRef: MatDialog;
  let tableApiSpySave;
  let tableApiSpyRemove;
  let dialogRefSpyAdd;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomTableComponent ],
      schemas: [ NO_ERRORS_SCHEMA ],
      providers: [
        { provide: TableApiService, useClass: TableApiServiceMock },
        { provide: MatDialog, useValue: {
          open: () => {
            return {
              afterClosed: () => {
                return {
                  subscribe: (res) => {
                    res()
                  },
                  unsubscribe: () => {}
                }
              }
            }
          }
        }}
      ],
      imports: [
        MatTableModule 
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomTableComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;
    tableApiService = debugElement.injector.get(TableApiService);
    matDialogRef = debugElement.injector.get(MatDialog);
    dialogRefSpyAdd = spyOn(matDialogRef, 'open').and.callThrough();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be addBranch button click', () => {
    const addButton = fixture.debugElement.query(By.css('button'));
    addButton.triggerEventHandler('click', null);
    expect(dialogRefSpyAdd).toHaveBeenCalledTimes(1);
  });

  it('should be editBranch button click', () => {
    fixture.detectChanges();
    component.editColumn(null);
    expect(dialogRefSpyAdd).toHaveBeenCalledTimes(1);
  });
});
